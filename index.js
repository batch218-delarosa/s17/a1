// console.log("Test");

/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function askForUserDetails() {
		let fullName = prompt("What is your full name?");
		let age = prompt("What is your age?");
		let location = prompt("What is your location");

		console.log("Hello, " + fullName);
		console.log(`You are ${age} years old.`);
		console.log("You live in " + location);


	}

	askForUserDetails();

	

 
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayFaveMovies() {
		let faveBands = ['Post Malone', 'Parokya Ni Edgar', "The Weekend", "Taylor Swift", "Kygo"];

		for (let i = 0; i < faveBands.length; i++) {
			console.log(`${(i+1)}. ${faveBands[i]}`);
		}

	}


	displayFaveMovies();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFavoriteMovies() {
		let faveMovies = ['Transformers', 'Transformers: Dark of the Moon', "Looper", "Jumper", "Fast & Furious"];
		let ratings = [58, 35, 93, 15, 28];

		for (let i = 0; i < faveMovies.length; i++) {
			console.log(`${(i+1)}. ${faveMovies[i]}`);
			console.log(`Rotten Tomatoes Rating: ${ratings[i]}%`)
		}

	}

	displayFavoriteMovies()



/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

 

printFriends();

// Comment out because these statements are trying to access variables in the printFriends function scope which isn't possible.
/*console.log(friend1);
console.log(friend2);*/
